# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

Ql = 494e6
S21_db = -20.3
S11_db = -18.2

S21 = 10**(S21_db/20)
S11 = 10**(S11_db/20)

Beta_1 = (1-S11)**2 / (1 - S11**2 - S21**2)
print("B1 =", Beta_1)

Beta_2 = (1+S11) / (1-S11) * Beta_1 -1
print("B2 =", Beta_2)

Q_0 = Ql * (1 + Beta_1 + Beta_2)
print("Q0 =", Q_0)