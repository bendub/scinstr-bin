#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""package n523x_utils
author    Benoit Dubois
copyright FEMTO ENGINEERING
license   GPL v3.0+
brief     Main script to track CSO mode during the warming with N5234
          or N5230 device.
"""

import logging
CONSOLE_LOG_LEVEL = logging.DEBUG
FILE_LOG_LEVEL = logging.DEBUG

# Ctrl-c closes the application
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

import sys
import argparse
import time
import os.path as path
import socket
import numpy as np
import scipy.signal as scs

import n523x as vna

LIMIT_PAUSE = 0.3
LIMIT_BW = 0.01
LIMIT_IFBW = 0.01
MINIMUM_SPAN = 10e6 # Hz
MINIMUM_IFBW = 10 # Hz
DEFAULT_PERIOD = 15 # minute
DEFAULT_PAUSE = 30 # second

APP_NAME = "ModeTracking"

#===============================================================================
class MyVna(vna.N523x):

    def connect(self, try_=3):
        """Overloaded vna.Vna method beacause, VNA seems to refuse connection
        without reasons.
        :param try_: number of connection attempt (int).
        """
        for i in range(try_):
            if super().connect() is True:
                break

#==============================================================================
def configure_logging():
    """Configures logs.
    """
    home = path.expanduser("~")
    log_file = "." + APP_NAME + ".log"
    abs_log_file = path.join(home, log_file)
    date_fmt = "%d/%m/%Y %H:%M:%S"
    log_format = "%(asctime)s %(levelname) -8s %(filename)s " + \
                 " %(funcName)s (%(lineno)d): %(message)s"
    logging.basicConfig(level=FILE_LOG_LEVEL, \
                        datefmt=date_fmt, \
                        format=log_format, \
                        filename=abs_log_file, \
                        filemode='w')
    console = logging.StreamHandler()
    # define a Handler which writes messages to the sys.stderr
    console.setLevel(CONSOLE_LOG_LEVEL)
    # set a format which is simpler for console use
    console_format = '%(levelname) -8s %(filename)s (%(lineno)d): %(message)s'
    formatter = logging.Formatter(console_format)
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

#==============================================================================
def parse_cmd_line():
    """Parses command line: allow user to choose a different ini file.
    :returns: parser object (object)
    """
    parser = argparse.ArgumentParser(description=APP_NAME)
    parser.add_argument('-ini', '--ini_file', dest='ini_file', \
                        type=str, default='', help='-ini=<str>')
    return parser.parse_args()

#==============================================================================
def get_pna_config(dev):
    w_num = dev.query("SYST:WIND:CAT?")
    all_meas_num = dev.query("SYST:MEAS:CAT?")
    return (w_num, all_meas_num)
        
#==============================================================================
def measure(dev, measurement_config):
    dev.get_measure(measurement_config[0])

#==============================================================================
def adjust_screen(dev, measurement_config, filter_=False):
    for w in measurement_config[0]:
        # Found mode
        data = dev.get_measure()
        datad = scs.detrend(data[:, 1]) # real(S11)
        #datad = scs.detrend(data[:, 3]) # real(S21)
        if filter_ is True:
            b, a = scs.butter(3, 0.005)
            datad = scs.filtfilt(b, a, datad, padlen=150)
        f_mod = data[np.argmin(datad), 0]
        # Check mode is valid
        # If check is true: center screen on mode
        dev.center_freq = f_mod
        # Else: try to change freq param to found extremum (use warming or cooling information)
        # TODO
        # THEN adjust span if needed
        span = dev.span
        if span > MINIMUM_SPAN / 2:
            bw_arg = np.argwhere(y < min(y)/1.414)
            if not bw_arg.size:
                bw = data[bw_arg[-1], 0] - data[bw_arg[0], 0]
                if float(bw / span) > LIMIT_BW:
                    dev.span = span / 2
        # THEN adjust IFBW if needed
        ifbw = dev.ifbw
        if ifbw > MINIMUM_IFBW / 2 and float(ifbw / span) > LIMIT_IFBW:            
            dev.ifbw = dev.IFBW[dev.IFBW.index(ifbw)-1]

#==============================================================================
def adjust_pause(current_pause, screen_param):
    # If screen_param has too changed, decrease pause
    ## -> |center_old - center_new| / span > LIMIT_PAUSE / 3
    # Else increase pause (limit to DEFAULT_PAUSE)
    pass

#==============================================================================
def main():
    """Main program of project.
    :returns: None
    """
    """args = parse_cmd_line()
    if path.isfile(args.ini_file) is True:
        check_ini_file(args.ini_file)
    else:
        check_ini_file()

    """

    vna = Vna(IP)

    if vna.connect() is False:
        return

    screen_param = (None, None)
    pause = DEFAULT_PAUSE
    measurement_config = get_pna_config()
    period = DEFAULT_PERIOD # in minute
    t0 = now()
    # Sets the data format for transferring measurement data and frequency data
    vna.write("FORMAT:DATA ASCII,0")
    # Specifies the format of subsequent .s1p, .s2p, .s3p; s4p save statements
    vna.write("MMEM:STOR:TRAC:FORM:SNP DB")
    
    bw = measure(vna, measurement_config)
    
    while True:
        screen_param = adjust_screen(vna)
        if t0 mod(now()) == period:
            measure(vna, measurement_config)
        pause = adjust_pause(pause, screen_param)
        time.delay(pause)


#==============================================================================
if __name__ == '__main__':
    configure_logging()
    main()
