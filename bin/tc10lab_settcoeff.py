#!/bin/env python3
import usbtmc
import textwrap
import logging
import argparse


VID=0x1a45
PID=0x3101


# =============================================================================
def parse_args():
    """Parse cli argument.
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description='''
        Utilities for Wavelenght Electronics TC10 LAB temperature controller:
        - set thermistor coefficient.
        - configure voltage output limit.
        ''')
    #
    cmdsubparser = parser.add_subparsers(title='subcommands',
                                         dest='cmd',
                                         description='valid sub commands',
                                         help='Query sub command')
    #
    thermparser = cmdsubparser.add_parser(
        'thermistor',
        formatter_class=argparse.RawTextHelpFormatter,
        help='Set thermistor coefficients (resistance=f(temperature)) to TC 10 LAB device.',
        epilog=textwrap.dedent('''
    Example: \'tc10lab-utils thermistor MBD10 25 10000 0 32651 -20 97001\'''')
    )
    thermparser.add_argument('name', help='Name of thermistor')
    thermparser.add_argument('t1', help='Temperature number 1')
    thermparser.add_argument('r1', help='Resistance number 1')
    thermparser.add_argument('t2', help='Temperature number 2')
    thermparser.add_argument('r2', help='Resistance number 2')
    thermparser.add_argument('t3', help='Temperature number 3')
    thermparser.add_argument('r3', help='Resistance number 3')
    #
    voltageparser = cmdsubparser.add_parser(
        'outputlimit',
        formatter_class=argparse.RawTextHelpFormatter,
        help='Configure voltage output limit of controller',
        epilog=textwrap.dedent('''
    Example: \'tc10lab-utils outputlimit 10
        Set voltage output limit to 10 Volts
        '''))
    voltageparser.add_argument('profile',
                               type=int,
                               choices=range(1,11),
                               help='Profile number')
    voltageparser.add_argument('volimit',
                               type=float,
                               help='Output Limit (in Volt)')

    return parser.parse_args()

# ===================================================================
def set_thermistor_coeff(dev, t1, r1, t2, r2, t3, r3):
    print("Sensor list before:", dev.ask("CONST:LIST?"))
    dev.write(f"CONST:THERM {name}, {t1}, {r1}, {t2}, {r2}, {t3}, {r3}")
    print("Sensor list now:", dev.ask("CONST:LIST?"))


# ===================================================================
def set_voltage_output_limit(dev, profile, limit):
    print
    print("Voltage output limit before:", dev.ask(f"PROFile:VLIM? {profile}"))
    #if 9 < limit < 18.33:
    #    dev.write(f"PROFile:VLIM {profile}, {limit}")
    print("Voltage output limit now:", dev.ask(f"PROFile:VLIM? {profile}"))


# ===================================================================
def main(args):
    dev = usbtmc.Instrument(VID, PID)
    print(dev.ask("*IDN?"))

    if args.cmd == 'thermistor':
        set_thermistor_coeff(dev,
                             args.t1, args.r1,
                             args.t2, args.r2,
                             args.t3, args.r3)
    elif args.cmd == 'outputlimit':
        set_voltage_output_limit(dev, args.profile, args.volimit)

    dev.write("LOCAL")
    dev.close()


# ===================================================================
date_fmt = "%d/%m/%Y %H:%M:%S"
log_format = "%(asctime)s %(levelname) -8s %(filename)s " + \
    " %(funcName)s (%(lineno)d): %(message)s"
logging.basicConfig(level=logging.DEBUG,
                    datefmt=date_fmt,
                    format=log_format)

args = parse_args()

main(args)
