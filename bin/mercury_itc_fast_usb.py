#!/usr/bin/env python3

"""package cryocon
This program get temperature from Cryocon 24C device.
Note on Cryocon 24C specifications:
- Input sampling rate: Typically 1 Hz, maximum 10 Hz (p.161 of manual)
- Input resolution: 24 bits
- Output resolution: 16 bis
Benoit Dubois (2019)
Licence GPL 3.0+
"""

import os
import argparse
import logging
import re
import time
import datetime
from collections import deque
import serial

import mercury.mercury_itc as itc

# USB connection
BAUDRATE = 115200
DATA_BITS = serial.EIGHTBITS
STOP_BIT = serial.STOPBITS_ONE
PARITY = serial.PARITY_NONE
USB_TIMEOUT = 0.5


# Default file name (year month day - hour minute second)
OFILENAME = 'cryocon' # File header
VAL_TYPE = 'RES' # Default value type


# Method to extract floats in a string.
# See https://stackoverflow.com/questions/4703390/how-to-extract-a-floating-number-from-a-string
# Return a list of floats contained in the string.
NUMERIC_CONST_PATTERN = r"""
     [-+]? # optional sign
     (?:
         (?: \d* \. \d+ ) # .1 .12 .123 etc 9.1 etc 98.1 etc
         |
         (?: \d+ \.? ) # 1. 12. 123. etc 1 12 123 etc
     )
     # followed by optional exponent part if desired
     (?: [Ee] [+-]? \d+ ) ?
     """
RX = re.compile(NUMERIC_CONST_PATTERN, re.VERBOSE)


#==============================================================================
def parse():
    """Specific parsing procedure for transfering data trace from Lakeshore
    temperature controler.
    :returns: populated namespace (parser)
    """
    parser = argparse.ArgumentParser( \
        description='Acquire data from Cryocon temperature controler'
        'connected through Ethernet.',
        epilog='Example: \'cryocon_fast_eth 192.168.0.22 -o toto\' configure '
        'cryocon_fast_eth with output file \'toto.dat\'')
    parser.add_argument('ip')
    parser.add_argument('-o', action='store', dest='ofile',
                        default=OFILENAME, \
                        help='Output data filename (default '+OFILENAME+')')
    parser.add_argument('-v', action='store', dest='vtype',
                        default=VAL_TYPE,
                        help='Value type: TEMP, RTMP, CTMP, VOLT, RES,'
                        ' (default ' + VAL_TYPE + ')')
    args = parser.parse_args()
    return args

#====================================================================
def check_error(dev):
    """Checks if error occured in device.
    :param sock: socket related to connection with the controller (object)
    :returns: None
    """
    dev.write("ERR?")
    try:
        error = dev.read(100)
    except Exception as ex:
        logging.error("%r", ex)
        raise
    logging.info("Device error: %r", error)

#====================================================================
def get_id(dev):
    """Returns identification string of device.
    :param sock: communication socket toward device (socket)
    :returns: identification string of device (str)
    """
    dev.settimeout(0.8)
    dev.write("*IDN?")
    _id = dev.read(100)
    return _id

#=================================================
def reset(dev):
    """Reset Controler.
    :param sock: socket related to connection with the controller (object)
    :returns: None
    """
    dev.write("SYS:RST")
    time.sleep(1)

#====================================================================
def set_man_output(dev, out_val, out_nb=1):
    """Set a manual output heater value on the Lakeshore
    :param sock: socket related to connection with the controller (object)
    :param output: the value of the manual heater output (float)
    :returns: None
    """
    cmd = "LOOP{}:PMAN {}".format(out_nb, out_val)
    dev.write(cmd)

#====================================================================
def read_data(dev, cmd):
    """Return data (basic).
    :param sock: socket related to connection with the controller (object)
    :param cmd: data query command sended to device (str)
    :returns: data write (str)
    """
    try:
        dev.write(cmd)
        data = dev.read()
    except Exception as ex:
        logging.error("%r", ex)
        raise
    return data

#====================================================================
def acquisition(dev, data_file, vtype):
    if vtype == 'TEMP': # Measured temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:TEMP"
    elif vtype == 'RTMP':  # Raw temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:RTMP"
    elif vtype == 'CTMP':  # Control temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:CTMP"
    elif vtype == 'VOLT':  # Sensor voltage
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:VOLT"
    else: # Measured resistance (PTC/NTC)
        cmd = 'READ:DEV:DB6.T1:TEMP:SIG:RES'
    while True:
        try:
            try:
                retval = read_data(dev, cmd)
                db_num, sens_num, value = RX.findall(retval)
            except Exception as ex:
                logging.error("Error when reading data %r", ex)
                continue
            now = datetime.datetime.utcnow()
            timetag = now.strftime("%Y%m%d-%H%M%S.%f")
            sample = timetag + '\t' + value + '\n'
            try:
                data_file.write(sample) # Write in a file
            except Exception as ex:
                logging.error("Error when writing data %r", ex)
                continue
        except KeyboardInterrupt:
            break # To stop the loop in a clean way

#====================================================================
def acquisition_once(dev, data_file, vtype, test_value=1):
    if vtype == 'TEMP': # Measured temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:TEMP"
    elif vtype == 'RTMP':  # Raw temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:RTMP"
    elif vtype == 'CTMP':  # Control temperature
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:CTMP"
    elif vtype == 'VOLT':  # Sensor voltage
        cmd = "READ:DEV:DB6.T1:TEMP:SIG:VOLT"
    else: # Measured resistance (PTC/NTC)
        cmd = 'READ:DEV:DB6.T1:TEMP:SIG:RES'
    prev_value = 0
    curr_idx = 0
    mean_idx_list = deque([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    mean_idx = 0
    while True:
        try:
            try:
                retval = read_data(dev, cmd)
                db_num, sens_num, value = RX.findall(retval)
            except Exception as ex:
                logging.error("Error when reading data %r", ex)
                continue
            # Check unicity of value
            #  Test new value
            if value == prev_value:
                # If it was a long time that there is no new value,
                # we consider that this is a new value.
                if not (mean_idx != 0 and curr_idx > mean_idx + test_value):
                    curr_idx += 1
                    continue
            #
            print(value,
                  prev_value,
                  curr_idx,
                  mean_idx_list,
                  mean_idx)
            mean_idx_list.append(curr_idx)
            mean_idx_list.popleft()
            curr_idx = 0
            prev_value = value
            #
            now = datetime.datetime.utcnow()
            timetag = now.strftime("%Y%m%d-%H%M%S.%f")
            sample = timetag + '\t' + value + '\n'
            try:
                data_file.write(sample) # Write in a file
            except Exception as ex:
                logging.error("Error when writing data %r", ex)
                continue
            #  Process new mean_idx
            mean_idx = 0
            for idx in mean_idx_list:
                if idx == 0:  # Exit if "mean_idx" element is 0
                    break
                mean_idx += idx
            mean_idx = mean_idx / len(mean_idx_list)
        except KeyboardInterrupt:
            break # To stop the loop in a clean way


#====================================================================
def main():
    """Main script.
    """
    args = parse()
    ip = args.ip
    ofile = args.ofile
    vtype = args.vtype
    filename = time.strftime("%Y%m%d-%H%M%S", time.localtime()) \
               + '-' + ofile + '.dat'
    #
    dev = itc.MercuryItcEth(ip)
    dev.connect()
    #reset(dev)
    with open(filename, 'w') as fd:
        acquisition_once(dev, fd, vtype)
    logging.info('--> Disconnected')
    #
    ans = input('Would you like to keep this datafile (y/n : default y)?')
    if ans == 'n':
        os.remove(filename)
        logging.info('%s removed', filename)
    else:
        logging.info('%s saved', filename)
    logging.info('Program ending')

#==============================================================================
if __name__ == "__main__":
    LOG_FORMAT = '%(asctime)s %(levelname)s %(filename)s (%(lineno)d): ' \
        +'%(message)s'
    logging.basicConfig(format=LOG_FORMAT, level=logging.INFO)
    main()
