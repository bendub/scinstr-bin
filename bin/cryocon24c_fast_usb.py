#!/usr/bin/env python3

"""package cryocon
This program get temperature from Cryocon 24C device.
Note on Cryocon 24C specifications:
- Input sampling rate: 15 Hz (p.22 of User's Guide)
- Excitation frequency of resistor sensor: 7.5 Hz (p.22 and 23 User's Guide)
(????? 10 Hz for PTC p.31 of User's Guide ?????)
- Loop Update Rate: 15 Hz per loop (p.25 of User's Guide)
- Input resolution: 24 bits
- Output resolution: 20 bis
Benoit Dubois (2019)
Licence GPL 3.0+
"""

import os
import sys
import argparse
import logging
import serial
import io
import numpy as np
import time
import datetime


# USB connection
BAUDRATE = 115200
DATA_BITS = serial.EIGHTBITS
STOP_BIT = serial.STOPBITS_ONE
PARITY = serial.PARITY_NONE
USB_TIMEOUT = 0.5


# Default file name (year month day - hour minute second)
OFILENAME = 'cryocon' # File header
VAL_TYPE = '' ##'TEMP' # Default value type


#====================================================================
class CryoconUsb(object):

    def __init__(self, port):
        self._ser = serial.Serial(port,
                                  baudrate=BAUDRATE,
                                  bytesize=DATA_BITS,
                                  parity=PARITY,
                                  stopbits=STOP_BIT,
                                  timeout=USB_TIMEOUT)
        ##self._ser = io.TextIOWrapper(io.BufferedRWPair(ser, ser))

    def write(self, data):
        time.sleep(0.050)  # 50 ms minimum between USB com (p.154 manual)
        return self._ser.write((data + '\n').encode('utf-8'))

    def read(self, length=1000):
        time.sleep(0.050)  # 50 ms minimum between USB com (p.154 manual)
        return self._ser.read(length).decode('utf-8').strip('\n')

    def query(self, data):
        self.write(data)
        retrun self.read()

    def readline(self):
        time.sleep(0.050)  # 50 ms minimum between USB com (p.154 manual)
        return self._ser.readline().decode('utf-8').strip('\n')


#==============================================================================
def configure_logging():
    FILE_LOG_LEVEL = logging.DEBUG
    CONSOLE_LOG_LEVEL = logging.DEBUG
    home = os.path.expanduser("~")
    log_file = "." + 'cryocon_fast_usb' + ".log"
    abs_log_file = os.path.join(home, log_file)
    date_fmt = "%d/%m/%Y %H:%M:%S"
    log_format = "%(asctime)s %(levelname) -8s %(filename)s " + \
                 " %(funcName)s (%(lineno)d): %(message)s"
    logging.basicConfig(level=FILE_LOG_LEVEL, \
                        datefmt=date_fmt, \
                        format=log_format, \
                        filename=abs_log_file, \
                        filemode='w')
    console = logging.StreamHandler()
    # define a Handler which writes messages to the sys.stderr
    console.setLevel(CONSOLE_LOG_LEVEL)
    # set a format which is simpler for console use
    console_format = '%(levelname) -8s %(filename)s (%(lineno)d): %(message)s'
    formatter = logging.Formatter(console_format)
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

#==============================================================================
def parse():
    """Specific parsing procedure for transfering data trace from Lakeshore
    temperature controler.
    :returns: populated namespace (parser)
    """
    parser = argparse.ArgumentParser( \
        description='Acquire data from Cryocon temperature controler'
        'connected through USB.',
        epilog='Example: \'cryocon_fast_usb /dev/ttyUSB0 -o toto\' configure '
        'cryocon_fast_usb with output file \'toto.dat\'')
    parser.add_argument('port')
    parser.add_argument('-o', action='store', dest='ofile',
                        default=OFILENAME, \
                        help='Output data filename (default '+OFILENAME+')')
    parser.add_argument('-v', action='store', dest='vtype',
                        default=VAL_TYPE, help='Value type TEMP or RES'
                        ' (default '+VAL_TYPE+')')
    args = parser.parse_args()
    return args

#====================================================================
def check_error(dev):
    """Checks if error occured in device.
    :param sock: socket related to connection with the controller (socket object)
    :returns: None
    """
    dev.write("ERR?")
    try:
        error = dev.read(100)
    except Exception as ex:
        logging.error("%r", ex)
        raise
    logging.info("error:", error)

#====================================================================
def get_id(dev):
    """Returns identification string of device.
    :param sock: communication socket toward device (socket)
    :returns: identification string of device (str)
    """
    dev.settimeout(0.8)
    dev.write("*IDN?")
    _id = dev.read(100)
    return _id

#=================================================
def reset(dev):
    """Reset Controler.
    :param sock: socket related to connection with the controller (object)
    :returns: None
    """
    dev.write("*RST")
    time.sleep(1)

#====================================================================
def set_man_output(dev, out_val, out_nb=1):
    """Set a manual output heater value on the Lakeshore
    :param sock: socket related to connection with the controller (object)
    :param output: the value of the manual heater output (float)
    :returns: None
    """
    cmd = "LOOP{}:PMAN {}".format(out_nb, out_val)
    dev.write(cmd)

#====================================================================
def read_data(dev, cmd):
    """Return data (basic).
    :param sock: socket related to connection with the controller (object)
    :param cmd: data query command sended to device (str)
    :returns: data write (str)
    """
    try:
        dev.write(cmd)
        data = dev.readline()
    except Exception as ex:
        logging.error("%r", ex)
        raise
    return data

#====================================================================
def read_data_checked(dev, cmd, timeout=100):
    """Return data with data unicity
    (see page 113 and 153).
    :param dev: device instance of controller (object)
    :param cmd: data query command sended to device (str)
    :returns: data readed (str)
    """
    dev.query('*ESR?')
    full_cmd = cmd + ';*OPC'
    dev.write(full_cmd)
    exception_msg = 'Writing with OPCsync - Timeout occured. Command: "{}", timeout {} ms'.format(cmd, timeout)
    _stb_polling(dev, exception_msg, timeout)
    dev.query('*ESR?')
    return dev.readline()

def _stb_polling(device, exception_msg, timeout_ms):
    """STB polling loop internal function
    """
    timeout_secs = timeout_ms / 1000
    start = time.time()
    # STB polling loop
    while True:
        # stb = device.read_stb()
        device.write("*STB?")
        stb = device.read()
        if (stb & 32) > 0:
            break

        elapsed = time.time() - start
        if elapsed > timeout_secs:
            raise InstrumentTimeoutException(exception_msg)
        else:
            # Progressive delay
            if elapsed > 0.01:
                if elapsed > 0.1:
                    if elapsed > 1:
                        if elapsed > 10:
                            time.sleep(0.5)
                        else:
                            time.sleep(0.1)
                    else:
                        time.sleep(0.01)
                else:
                    time.sleep(0.001)

#====================================================================
def acquisition(dev, data_file, vtype):
    while True:
        try:
            if vtype == 'TEMP': # Temperature value
                # Reading is filtered by the display time constant filter
                cmd = "INP:TEMP?"
            else:
                # Reading is not filtered by the display time-constant filter.
                # However, the synchronous input filter has been applied.
                # (Synchronous Filter Configuration?)
                cmd = "INP:SENP?"
            try:
                retval = read_data(dev, cmd)
            except Exception as ex:
                logging.error("Error when reading data %r", ex)
                continue
            data = retval.split(',')
            now = datetime.datetime.utcnow()
            timetag = now.strftime("%Y%m%d-%H%M%S.%f")
            sample = timetag + '\t' + data[0] + '\t' + data[1] \
                     + '\t' + data[1] + '\t' + data[3] + '\n'
            try:
                data_file.write(sample) # Write in a file
            except Exception as ex:
                logging.error("Error when writing data %r", ex)
                continue
        except KeyboardInterrupt:
            break # To stop the loop in a clean way

#====================================================================
def main():
    """Main script.
    """
    args = parse()
    port = args.port
    ofile = args.ofile
    vtype = args.vtype
    filename = time.strftime("%Y%m%d-%H%M%S", time.localtime()) \
               + '-' + ofile + '.dat'
    #
    dev = CryoconUsb(port)
    reset(dev)
    with open(filename, 'w') as fd:
        acquisition(dev, fd, vtype)
    dev.write("LCL") # Set device in local mode
    logging.info('--> Disconnected')
    #
    ans = input('Would you like to keep this datafile (y/n : default y)?')
    if ans == 'n':
        os.remove(filename)
        logging.info('%s removed', filename)
    else:
        logging.info('%s saved', filename)
    logging.info('Program ending')

#==============================================================================
if __name__ == "__main__":
    configure_logging()
    main()
