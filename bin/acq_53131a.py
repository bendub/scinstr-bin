import iopy.prologix as px
import time

counter = px.PrologixGpibEth("192.168.0.30", 1234, 113)
counter.init()

#counter.write("CONF:FREQ 11.6E6")
#counter.write("TRIG:SOUR IMM")
#counter.write("TRIG:SLOP POS")
#counter.write("SENS:FREQ:GATE:TIME 1.0")
#counter.write("SENS:FREQ:GATE:SOUR TIME")
#print("Error config?:", counter.get_error())

try:
    while True:
        counter.write("DATA?")
        value = counter.read(100)[:-1]
        print(value)
        time.sleep(1.0)
except KeyboardInterrupt:
    counter.write("ABORT")
except Exception as er:
    print("# Exception during acquisition: %r", er)

counter.close()
